﻿namespace Skydra.StateMachine.IDs
{
    [UnityEngine.CreateAssetMenu(menuName = "Game/ID/Stance", fileName = "New Stance ID", order = -1000)]
    public sealed class StanceID : IDs
    {
        public string stanceName;

        private void OnValidate()
        {
            if (string.IsNullOrEmpty(stanceName)) stanceName = name;
        }
    }
}