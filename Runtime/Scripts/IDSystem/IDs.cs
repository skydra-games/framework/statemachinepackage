﻿using UnityEngine;

namespace Skydra.StateMachine.IDs
{
    public abstract class IDs : ScriptableObject
    {
        [Tooltip("Integer value to Identify IDs")]
        public int ID;

        public static implicit operator int(IDs reference) => reference != null ? reference.ID : 0; //  =>  reference.ID;

    }
}