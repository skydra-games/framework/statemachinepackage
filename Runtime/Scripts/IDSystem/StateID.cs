﻿namespace Skydra.StateMachine.IDs
{
    [UnityEngine.CreateAssetMenu(menuName = "Game/ID/State", fileName = "New State ID", order = -1000)]
    public sealed class StateID : IDs
    {
        public string stateName;

        private void OnValidate()
        {
            if (string.IsNullOrEmpty(stateName)) stateName = name;
        }
    }
}