﻿using System.Linq;

namespace Skydra.StateMachine.Character
{
    public readonly partial struct CharacterStatus
    {
        public const int Idle = 0;
        public const int Moving = 1;
        public const int Attacking = 2;
        public const int AttackingEnded = 3;
        public const int RecoveryAfterAttack = 4;
        public const int Dead = 5;
    }
}