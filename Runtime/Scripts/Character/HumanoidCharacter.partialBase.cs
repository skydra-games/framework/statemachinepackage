﻿namespace Skydra.StateMachine.Character
{
    public abstract partial class HumanoidCharacter : StateBasedCharacter
    {
        #region Update

        protected virtual void Update()
        {
            Sm.CheckStates();
            if (!Sm.CurrentState)
                return;
            Sm.CurrentState.HandleInput();
            Sm.CurrentState.LogicUpdate();
        }

        protected virtual void FixedUpdate()
        {
            if (!Sm.CurrentState)
                return;
            Sm.CurrentState.PhysicsUpdate();
        }
        
        protected virtual void OnAnimatorMove()
        {
            if (!Sm.CurrentState)
                return;
            Sm.CurrentState.RootMotion();
        }

        #endregion
    }
}