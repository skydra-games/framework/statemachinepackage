﻿namespace Skydra.StateMachine.Character
{
    public readonly partial struct CharacterStatus
    {
        private int Index { get; }

        private CharacterStatus(int index)
        {
            Index = index;
        }

        public static implicit operator CharacterStatus(int index) => new(index);
        public static implicit operator int(CharacterStatus status) => status.Index;

        
        public static bool operator ==(CharacterStatus c1, CharacterStatus c2) => c1.Index == c2.Index;
        public static bool operator !=(CharacterStatus c1, CharacterStatus c2) => c1.Index != c2.Index;
    }
}