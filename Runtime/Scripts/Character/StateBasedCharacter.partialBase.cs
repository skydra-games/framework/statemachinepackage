using System;
using System.Collections.Generic;
using Skydra.Helper;
using Skydra.Helper.Attributes;
using Skydra.StateMachine.States.Character;
using UnityEngine;

namespace Skydra.StateMachine.Character
{
    [RequireComponent(typeof(CharacterController))]
    public abstract partial class StateBasedCharacter : MonoBehaviour
    {
        public IInputAdapter InputAdapter;
        public Animator animator;
        public CharacterController cc;
        public bool debuggingMode;
        
        protected StateMachine Sm;

        [ReadOnly] private CharacterStatus _currentStatus;
     
        public event Action<CharacterStatus> OnStatusChanged;

        public CharacterStatus CurrentStatus
        {
            get => _currentStatus;
            set
            {
                _currentStatus = value;
                OnStatusChanged?.Invoke(_currentStatus);
            }
        }
        
        [SerializeField, Tooltip("Priority order of states is based on list order.")] private List<CharacterState<StateBasedCharacter>> states;

        public void Initialize()
        {
            
            if (InputAdapter == null && TryGetComponent(out IInputAdapter inputAdapter))
            {
                InputAdapter = inputAdapter;
                InputAdapter.Initialize();
            }
            if (cc == null && TryGetComponent(out CharacterController ccComponent))
                cc = ccComponent;
            
            
            Sm = new StateMachine();

            for (int i = 0; i < states.Count; i++)
            {
                states[i] = Instantiate(states[i]);
                states[i].Initialize(this);
                Sm.AddState(states[i].id.stateName, states[i]);
            }
            
            Sm.Initialize(debuggingMode);
        }
        
        public bool SetState(string stateName)
        {
            return Sm.TryChangeState(stateName);
        }
    }
}