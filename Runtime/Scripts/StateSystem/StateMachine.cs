using System.Collections.Generic;
using UnityEngine;

namespace Skydra.StateMachine
{
    public class StateMachine
    {
        public State CurrentState;
        public string CurrentStateKey;
        private Dictionary<string, State> states = new Dictionary<string, State>();
        private bool _debugging;

        public void AddState(string name, State state)
        {
            states.TryAdd(name, state);
        }

        public void Initialize(bool debuggingMode = false)
        {
            _debugging = debuggingMode;
        }

        public void CheckStates()
        {
            foreach (string stateKey in states.Keys)
            {
                if (TryChangeState(stateKey))
                    break;
            }
        }

        public bool TryChangeState(string key)
        {
            if (states.ContainsKey(key) && states[key].IsAvailable())
            {
                State targetState = states[key];
                if (key != CurrentStateKey)
                {
                    if (CurrentState)
                    {
                        if (CurrentState.canExit)
                        {
                            if (_debugging)
                                Debug.Log("<color=#ff0000>SM: Exit from </color>" + CurrentStateKey);
                            CurrentState.Exit();
                        }
                        else
                        {
                            if (targetState.canForceTo.Contains(CurrentState.id))
                            {
                                if (_debugging)
                                    Debug.Log("<color=#orange>SM: Forced to exit from </color>" + CurrentStateKey);
                                CurrentState.ForceToExit();
                            }
                            else
                            {
                                if (_debugging)
                                    Debug.Log("<color=orange>SM: Can't exit from </color>" + CurrentStateKey);
                                return false;
                            }
                        }
                    }
                                    
                    CurrentState = states[key];
                    CurrentStateKey = key;
                    CurrentState.Enter();
                    if (_debugging)
                        Debug.Log("<color=#00ff00>SM: Enter to </color>" + CurrentStateKey);
                    return true;
                }

                return true;
            }
            
            return false;
        }
    }
}
