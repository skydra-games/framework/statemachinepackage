using System.Collections.Generic;
using Skydra.StateMachine.IDs;
using UnityEngine;

namespace Skydra.StateMachine
{
    public abstract class State : ScriptableObject
    {
        public StateID id;
        public List<StateID> canForceTo;
        public StateMachine StateMachine;
        [HideInInspector] public bool canExit = true;

        protected State(StateMachine stateMachine, bool canExit = true)
        {
            StateMachine = stateMachine;
            this.canExit = canExit;
        }

        public abstract bool IsAvailable();
        public abstract void Enter();
        public abstract void HandleInput();
        public abstract void LogicUpdate();
        public abstract void PhysicsUpdate();
        public abstract void RootMotion();
        public abstract void Exit();
        public abstract void ForceToExit();
    }
}
