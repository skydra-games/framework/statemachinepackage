using Skydra.StateMachine.Character;
using UnityEngine;

namespace Skydra.StateMachine.States.Character
{
    public abstract class CharacterState<T> : State where T : StateBasedCharacter
    {
        protected T character;
        protected static readonly int StateChange = Animator.StringToHash("StateChange");
        protected static readonly int StateId = Animator.StringToHash("State");
        protected static readonly int LastState = Animator.StringToHash("LastState");

        public CharacterState(T character, StateMachine stateMachine, bool canExit = true) : base(stateMachine, canExit)
        {
            this.character = character;
        }

        public virtual void Initialize(T newCharacter, bool canExit = true)
        {
            this.character = newCharacter;
            base.canExit = canExit;
        }

        public override void Enter()
        {
            character.animator.SetTrigger(StateChange);
            character.animator.SetInteger(StateId, id.ID);
        }

        public override void Exit()
        {
            character.animator.SetInteger(LastState, id.ID);
        }

        public override void ForceToExit()
        {
            Exit();
        }
    }
}