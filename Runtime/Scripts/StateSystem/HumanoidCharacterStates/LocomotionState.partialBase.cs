﻿using Skydra.StateMachine.Character;
using UnityEngine;

namespace Skydra.StateMachine.States.Character.Humanoid
{
    [CreateAssetMenu(menuName = "Game/State/Locomotion", fileName = "New Locomotion State", order = -1000)]
    public class LocomotionState : CharacterState<StateBasedCharacter>
    {
        public string moveInputID = "Move";
        public string lookInputID = "Look";
        public float rotateSpeed = 10;

        public bool strafe;
        
        private Vector3 _movementDirection;
        private Vector3 _lookDirection;
        private float _ySpeed;
        private static readonly int MoveX = Animator.StringToHash("MoveX");
        private static readonly int MoveY = Animator.StringToHash("MoveY");

        public LocomotionState(StateBasedCharacter character, StateMachine stateMachine, bool canExit = true) : base(character, stateMachine, canExit)
        {
        }

        public override bool IsAvailable()
        {
            return character.InputAdapter.GetInput<Vector2>(moveInputID).sqrMagnitude > 0;
        }
        
        public override void HandleInput()
        {
            var moveInput = character.InputAdapter.GetInput<Vector2>(moveInputID);
            _movementDirection = new Vector3(moveInput.x, 0, moveInput.y);
            
            if (strafe)
            {
                var lookInput = character.InputAdapter.GetInput<Vector2>(lookInputID);
                _lookDirection = new Vector3(lookInput.x, 0, lookInput.y);
            }
        }

        public override void LogicUpdate()
        {
            if (character.cc.isGrounded)
                _ySpeed = -0.5f;
            else
                _ySpeed += Physics.gravity.y * Time.deltaTime;
            
            if (strafe)
            {
                Quaternion rotation = Quaternion.LookRotation(_lookDirection, Vector3.up);
                character.transform.rotation = Quaternion.RotateTowards(character.transform.rotation, rotation,
                    rotateSpeed * Time.deltaTime);

                _movementDirection = character.transform.InverseTransformDirection(_movementDirection);
            }
            
            character.animator.SetFloat(MoveX, _movementDirection.x);
            character.animator.SetFloat(MoveY, _movementDirection.z);
            
            if (_movementDirection != Vector3.zero)
            {/*
                Quaternion rotation = Quaternion.LookRotation(_movementDirection, Vector3.up);
                character.transform.rotation = Quaternion.RotateTowards(character.transform.rotation, rotation,
                    rotateSpeed * Time.deltaTime);*/
            }
        }

        public override void PhysicsUpdate()
        {
            
        }

        public override void RootMotion()
        {
            Vector3 velocity = character.animator.deltaPosition;
            velocity.y = _ySpeed * Time.deltaTime;
            character.cc.Move(velocity);
        }
    }
}