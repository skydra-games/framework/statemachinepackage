﻿using Skydra.StateMachine.Character;
using UnityEngine;

namespace Skydra.StateMachine.States.Character.Humanoid
{
    [CreateAssetMenu(menuName = "Game/State/Move", fileName = "New Move State", order = -1000)]
    public class MoveState : CharacterState<StateBasedCharacter>
    {
        public string moveInputID = "Move";
        public string lookInputID = "Look";
        public float rotateSpeed = 10;
        public float moveSpeed = 10;
        public float acceleration = 10;
        private Vector3 velocity;

        public bool strafe;
        
        private Vector3 _movementDirection;
        private Vector3 _animMovementDirection;
        private Vector3 _lookDirection;
        private float _ySpeed;
        private static readonly int MoveX = Animator.StringToHash("MoveX");
        private static readonly int MoveY = Animator.StringToHash("MoveY");
        
        public MoveState(StateBasedCharacter character, StateMachine stateMachine, bool canExit = true) : base(character, stateMachine, canExit)
        {
        }

        public override bool IsAvailable()
        {
            return character.InputAdapter.GetInput<Vector2>(moveInputID).sqrMagnitude > 0;
        }

        public override void Enter()
        {
            base.Enter();
            character.CurrentStatus = CharacterStatus.Moving;
        }

        public override void HandleInput()
        {
            var moveInput = character.InputAdapter.GetInput<Vector2>(moveInputID);
            _movementDirection = new Vector3(moveInput.x, 0, moveInput.y);
            
            if (strafe)
            {
                var lookInput = character.InputAdapter.GetInput<Vector2>(lookInputID);
                _lookDirection = new Vector3(lookInput.x, 0, lookInput.y);
            }
        }

        public override void LogicUpdate()
        {
            if (character.cc.isGrounded)
                _ySpeed = -0.5f;
            else
                _ySpeed += Physics.gravity.y * Time.deltaTime;
            
            velocity = Vector3.MoveTowards(velocity, _movementDirection * moveSpeed, acceleration * Time.deltaTime);
            velocity = Vector3.ClampMagnitude(velocity, moveSpeed);
            if (velocity.sqrMagnitude < 0.1f)
                velocity = Vector3.zero;
            canExit = velocity.sqrMagnitude == 0;
            velocity.y = _ySpeed;
            
            if (strafe)
            {
                Quaternion rotation = Quaternion.LookRotation(_lookDirection, Vector3.up);
                character.transform.rotation = Quaternion.RotateTowards(character.transform.rotation, rotation,
                    rotateSpeed * Time.deltaTime);

                _animMovementDirection = character.transform.InverseTransformDirection(velocity.normalized);
            }


            float speedT = Mathf.InverseLerp(0, moveSpeed, velocity.magnitude);
            character.animator.SetFloat(MoveX, _animMovementDirection.x * speedT);
            character.animator.SetFloat(MoveY, _animMovementDirection.z * speedT);
            character.cc.Move(velocity * Time.deltaTime);
        }

        public override void PhysicsUpdate()
        {
            
        }

        public override void RootMotion()
        {
            
        }

        public override void Exit()
        {
            base.Exit();
            velocity = Vector3.zero;
            _ySpeed = 0f;
        }
    }
}