﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Skydra.StateMachine.Character;
using UnityEngine;

namespace Skydra.StateMachine.States.Character.Humanoid
{
    [CreateAssetMenu(menuName = "Game/State/Idle", fileName = "New Idle State", order = -1000)]
    public partial class IdleState : CharacterState<StateBasedCharacter>
    {
        public string lookInputID = "Look";
        private Vector3 _lookDirection;
        public float rotateSpeed = 10;
        private bool _isTurning = false;
        private static readonly int Turn = Animator.StringToHash("Turn");
        private static readonly int TurnID = Animator.StringToHash("TurnID");
        private CancellationTokenSource _tokenSource;

        public IdleState(StateBasedCharacter character, StateMachine stateMachine, bool canExit = true) : base(character, stateMachine, canExit)
        {
        }

        public override bool IsAvailable()
        {
            return true;
        }

        public override void Enter()
        {
            base.Enter();
            _isTurning = false;
            character.CurrentStatus = CharacterStatus.Idle;
        }

        public override void HandleInput()
        {
            var lookInput = character.InputAdapter.GetInput<Vector2>(lookInputID);
            _lookDirection = new Vector3(lookInput.x, 0, lookInput.y);
        }

        public override void LogicUpdate()
        {
            /*float angleDiff = Vector3.SignedAngle(character.transform.forward, _lookDirection, Vector3.up);
            if (!_isTurning)
            {
                if (angleDiff is >= 45 and <= 135)
                    TurnTo(0);
                else if (angleDiff >= 135)
                    TurnTo(2);
                else if (angleDiff is >= -135 and <= -45)
                    TurnTo(1);
                else if (angleDiff is <= -135)
                    TurnTo(3);
            }*/
            
            Quaternion rotation = Quaternion.LookRotation(_lookDirection, Vector3.up);
            character.transform.rotation = Quaternion.RotateTowards(character.transform.rotation, rotation,
                rotateSpeed * Time.deltaTime);
        }

        public override void PhysicsUpdate()
        {

        }

        public override void RootMotion()
        {
            character.transform.rotation = character.animator.rootRotation;
            Vector3 velocity = character.animator.deltaPosition;
            character.cc.Move(velocity);
        }

        public override void Exit()
        {
            base.Exit();
        }

        private async void TurnTo(int index)
        {
            _isTurning = true;
            character.animator.SetInteger(TurnID, index);
            character.animator.SetTrigger(Turn);
            
            _tokenSource = new CancellationTokenSource();
            var token = _tokenSource.Token;
            try
            {
                await Task.Delay(1000, token);
            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                _tokenSource.Dispose();
            }
            _isTurning = false;
        }
    }
}