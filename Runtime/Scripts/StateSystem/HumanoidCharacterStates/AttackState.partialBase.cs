using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Skydra.Helper.Data;
using Skydra.StateMachine.Character;
using UnityEngine;

namespace Skydra.StateMachine.States.Character.Humanoid
{
    [CreateAssetMenu(menuName = "Game/State/Attack", fileName = "New Attack State", order = -1000)]
    public class AttackState : CharacterState<StateBasedCharacter>
    {
        public int comboCount;
        private int _currentComboId;
        private bool _backupNextAttack;
        private bool _attackInput;
        
        public string lookInputID = "Look";
        public List<int> canLookOn;
        private Vector3 _lookDirection;
        public float rotateSpeed = 10;
        
        private float _ySpeed;

        private static readonly int ComboID = Animator.StringToHash("ComboID");
        private static readonly int RotationFlexibility = Animator.StringToHash("RotationFlexibility");
        
        private CancellationTokenSource _tokenSource;

        public AttackState(StateBasedCharacter character, StateMachine stateMachine, bool canExit = true) : base(character, stateMachine, canExit)
        {
            
        }

        public override bool IsAvailable()
        {
            return character.InputAdapter.GetInput<bool>("Attack");
        }

        public override void Enter()
        {
            character.OnStatusChanged += OnCharacterStatusChanged;
            character.animator.SetInteger(StateId, id.ID);
            _currentComboId = 0;
            Attack();
            if (character.InputAdapter.TryGetInputData("Attack", out InputData inputData))
                inputData.OnChange += OnAttackInput;
        }

        public override void HandleInput()
        {
            var lookInput = character.InputAdapter.GetInput<Vector2>(lookInputID);
            _lookDirection = new Vector3(lookInput.x, 0, lookInput.y);
        }

        public override void LogicUpdate()
        {
            if (character.cc.isGrounded)
                _ySpeed = -0.5f;
            else
                _ySpeed += Physics.gravity.y * Time.deltaTime;
            
            if (_attackInput)
            {
                _attackInput = false;
                Attack();
            }
            
            if (canLookOn.Contains(character.CurrentStatus))
            {
                Quaternion rotation = Quaternion.LookRotation(_lookDirection, Vector3.up);
                character.transform.rotation = Quaternion.RotateTowards(character.transform.rotation, rotation,
                    rotateSpeed * character.animator.GetFloat(RotationFlexibility) * Time.deltaTime);
            }
        }

        public override void PhysicsUpdate()
        {
        }

        public override void RootMotion()
        {
            Vector3 velocity = character.animator.deltaPosition;
            velocity.y += _ySpeed * Time.deltaTime;
            character.cc.Move(velocity);
        }

        public override void Exit()
        {
            character.OnStatusChanged -= OnCharacterStatusChanged;
            if (character.InputAdapter.TryGetInputData("Attack", out InputData inputData))
                inputData.OnChange -= OnAttackInput;
            base.Exit();
            _currentComboId = 0;
            _backupNextAttack = false;
        }

        public override void ForceToExit()
        {
            base.ForceToExit();
        }

        private void Attack()
        {
            if (character.CurrentStatus == CharacterStatus.AttackingEnded)
                return;
            
            if (character.CurrentStatus == CharacterStatus.Attacking)
            {
                if (!_backupNextAttack)
                    _backupNextAttack = true;
                return;
            }
            
            canExit = false;
            _currentComboId = Mathf.Clamp(_currentComboId, 0, comboCount);
            character.animator.SetInteger(ComboID, _currentComboId);
            character.animator.SetTrigger(StateChange);
            character.CurrentStatus = CharacterStatus.Attacking;
            if (_currentComboId >= comboCount - 1)
                _currentComboId = 0;
            else
                _currentComboId++;
        }
        
        private void OnCharacterStatusChanged(CharacterStatus status)
        {
            switch (status)
            {
                case CharacterStatus.RecoveryAfterAttack:
                    if (_backupNextAttack)
                    {
                        _attackInput = true;
                        _backupNextAttack = false;
                    }
                    break;
                case CharacterStatus.AttackingEnded:
                    FinishAttacking();
                    break;
            }
        }
        
        private void OnAttackInput(object arg0)
        {
            _attackInput = (bool)arg0;
        }

        private async void FinishAttacking()
        {
            _tokenSource = new CancellationTokenSource();
            var token = _tokenSource.Token;
            try
            {
                await Task.Delay(100, token);
            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                _tokenSource.Dispose();
            }
            canExit = true;
        }
    }
}