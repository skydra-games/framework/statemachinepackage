﻿using Skydra.StateMachine.Character;
using UnityEngine;

namespace Skydra.StateMachine.States.Character.Humanoid
{
    [CreateAssetMenu(menuName = "Game/State/Fall", fileName = "New Fall State", order = -1000)]
    public class FallState : CharacterState<StateBasedCharacter>
    {
        public FallState(StateBasedCharacter character, StateMachine stateMachine, bool canExit = true) : base(character, stateMachine, canExit)
        {
        }

        public override bool IsAvailable()
        {
            return !character.cc.isGrounded && character.cc.velocity.y < 0;
        }

        public override void Enter()
        {
            base.Enter();
        }

        public override void HandleInput()
        {
        }

        public override void LogicUpdate()
        {
        }

        public override void PhysicsUpdate()
        {
        }

        public override void RootMotion()
        {
            
        }
    }
}