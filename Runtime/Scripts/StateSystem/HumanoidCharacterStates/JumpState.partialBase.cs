﻿using Skydra.StateMachine.Character;
using UnityEngine;

namespace Skydra.StateMachine.States.Character.Humanoid
{
    [CreateAssetMenu(menuName = "Game/State/Jump", fileName = "New Jump State", order = -1000)]
    public class JumpState : CharacterState<StateBasedCharacter>
    {
        public string jumpInputID;
        public JumpState(StateBasedCharacter character, StateMachine stateMachine, bool canExit = true) : base(character, stateMachine, canExit)
        {
        }
        
        public override bool IsAvailable()
        {
            return character.InputAdapter.GetInput<bool>(jumpInputID);
        }

        public override void Enter()
        {
            base.Enter();
        }

        public override void HandleInput()
        {

        }

        public override void LogicUpdate()
        {

        }

        public override void PhysicsUpdate()
        {

        }

        public override void RootMotion()
        {
            
        }
    }
}